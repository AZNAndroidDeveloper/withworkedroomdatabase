package uz.app.roomdatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.app.roomdatabase.databinding.ActivityMainBinding
import uz.app.roomdatabase.presentation.intro.IntroFragment

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction().replace(binding.frame.id,IntroFragment()).commit()
    }
}