package uz.app.roomdatabase.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.app.roomdatabase.database.entity.MyEntity
import uz.app.roomdatabase.presentation.model.Users
import uz.app.roomdatabase.databinding.UserItemBinding as UserBinding

class UserAdapter( private val onClickListener:(MyEntity)->Unit ):RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
val userlist = mutableListOf<MyEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
   return UserViewHolder(UserBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int = userlist.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val elements = userlist[position]
   holder.bind(elements)
    }

    inner class UserViewHolder(val binding: UserBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(users: MyEntity){
            with(binding){
                tvUserName.text = users.username
                tvCountry.text = users.country
                linear.apply {
                    setOnClickListener {
                        onClickListener.invoke(users)
                    }
                }
            }


        }
    }
    fun setUsers(element:MutableList<MyEntity>){
        userlist.apply {  clear(); addAll(element) }
        notifyDataSetChanged()
    }
}