package uz.app.roomdatabase.presentation.intro

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Database
import uz.app.roomdatabase.R
import uz.app.roomdatabase.database.UserDB
import uz.app.roomdatabase.database.entity.MyEntity
import uz.app.roomdatabase.databinding.FragmentIntroBinding
import uz.app.roomdatabase.presentation.adapter.UserAdapter
import java.lang.Exception

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var database: UserDB
    private lateinit var myEntity: MyEntity
    private val userAdapter by lazy {
        UserAdapter(onClickListener = {
            Toast.makeText(requireContext(), it.username, Toast.LENGTH_SHORT).show()
            myEntity = it
        })
    }
    private lateinit var binding: FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        database = UserDB.getInstance(requireContext())
        with(binding) {
            userAdapter.setUsers(database.myEntityDao().getAllUser())
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = userAdapter

            }
            btnAdd.apply {
                setOnClickListener {
                    if (userName.text.toString().isNotEmpty() && countryName.text.toString().isNotEmpty()) {
                        insertUser(userName.text.toString(), countryName.text.toString())
                        userName.text.clear()
                        countryName.text.clear()

                    } else {
                        Toast.makeText(requireContext(), "Malumotni toliq kiriting", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            btnDelete.apply {
                setOnClickListener {
                    deleteUser(myEntity)
                }
            }
        }
    }

    fun insertUser(name: String, country: String) {
        database.myEntityDao().insertUser(MyEntity(0, name, country))
        notifyChange()
    }

    fun deleteUser(myEntity: MyEntity) {

        try {

            database.myEntityDao().deleteUser(myEntity)
            notifyChange()

        } catch (ex: Exception) {
            ex.printStackTrace()

        }
    }

    fun notifyChange() {
        with(binding) {
            userAdapter.setUsers(database.myEntityDao().getAllUser())
            recyclerView.adapter = userAdapter
        }
    }
}