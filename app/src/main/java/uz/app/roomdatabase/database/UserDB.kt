package uz.app.roomdatabase.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.app.roomdatabase.database.dao.MyEntityDao
import uz.app.roomdatabase.database.entity.MyEntity

@Database(entities = [MyEntity::class],version = 1)
abstract  class UserDB():RoomDatabase(){
    abstract fun myEntityDao():MyEntityDao

    companion object{
        @Volatile
        private var INSTANCE:UserDB ?= null
        fun getInstance(context: Context):UserDB{
            synchronized(this){
                var instance = INSTANCE
                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,UserDB::class.java,"user_db")
                        .fallbackToDestructiveMigrationFrom()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance

                }
                return instance

            }
        }
    }
}