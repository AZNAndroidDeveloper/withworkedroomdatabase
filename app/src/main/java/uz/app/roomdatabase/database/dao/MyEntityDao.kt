package uz.app.roomdatabase.database.dao

import androidx.room.*
import uz.app.roomdatabase.database.entity.MyEntity

@Dao
interface MyEntityDao{
    @Insert
    fun insertUser(myEntity: MyEntity)

    @Update
    fun updateUser(myEntity: MyEntity)

    @Query("SELECT * FROM Users ORDER BY id DESC")
    fun getAllUser():MutableList<MyEntity>

    @Delete
    fun deleteUser(myEntity: MyEntity)
}